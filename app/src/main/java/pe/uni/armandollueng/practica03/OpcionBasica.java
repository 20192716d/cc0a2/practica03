package pe.uni.armandollueng.practica03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class OpcionBasica extends AppCompatActivity {

    TextView textView;
    Button button1, button2, button3,
           button4, button5, button6,
           button7, button8, button9,
           button0, buttonSuma, buttonResta,
           buttonPor,buttonDiv, buttonPoint,
           buttonClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opcion_basica);

        textView = findViewById(R.id.text_view_numbers);
        button0 = findViewById(R.id.button_cero);
        button1 = findViewById(R.id.button_uno);
        button2 = findViewById(R.id.button_dos);
        button3 = findViewById(R.id.button_tres);
        button4 = findViewById(R.id.button_cuatro);
        button5 = findViewById(R.id.button_cinco);
        button6 = findViewById(R.id.button_seis);
        button7 = findViewById(R.id.button_siete);
        button8 = findViewById(R.id.button_ocho);
        button9 = findViewById(R.id.button_nueve);
        buttonSuma = findViewById(R.id.button_suma);
        buttonResta = findViewById(R.id.button_resta);
        buttonPor = findViewById(R.id.button_por);
        buttonDiv = findViewById(R.id.button_division);
        buttonPoint = findViewById(R.id.button_point);
        buttonClear = findViewById(R.id.button_delete);

        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "0";
                textView.setText(n_numero);
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "1";
                textView.setText(n_numero);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "2";
                textView.setText(n_numero);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "3";
                textView.setText(n_numero);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "4";
                textView.setText(n_numero);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "5";
                textView.setText(n_numero);
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "6";
                textView.setText(n_numero);
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "7";
                textView.setText(n_numero);
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "8";
                textView.setText(n_numero);
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + "9";
                textView.setText(n_numero);
            }
        });

        buttonPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s_numero = textView.getText().toString();
                String n_numero = s_numero + ".";
                textView.setText(n_numero);
            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("");
            }
        });


    }
}