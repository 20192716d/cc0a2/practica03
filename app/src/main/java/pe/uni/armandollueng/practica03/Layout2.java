package pe.uni.armandollueng.practica03;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class Layout2 extends AppCompatActivity {

    RadioButton radioButtonBasica, radioButtonCientifica, radioButtonProgramador;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout2);

        radioButtonBasica = findViewById(R.id.radio_button_1);
        radioButtonCientifica = findViewById(R.id.radio_button_2);
        radioButtonProgramador = findViewById(R.id.radio_button_3);
        button = findViewById(R.id.button_start);

        button.setOnClickListener(v -> {
            if(!(radioButtonBasica.isChecked() || radioButtonCientifica.isChecked() || radioButtonProgramador.isChecked())){
                Snackbar.make(v, R.string.msg_snack_bar, Snackbar.LENGTH_INDEFINITE).setAction(R.string.close, v1 -> {
                }).show();
                return;
            }

            if(radioButtonBasica.isChecked()){
                Intent intent = new Intent(Layout2.this, OpcionBasica.class);
                startActivity(intent);
                finish();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(Layout2.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(R.string.dialog_msg);
                builder.setPositiveButton("Si", (dialog, which) -> {
                });
                builder.setNegativeButton("No", (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());//matar el proceso
                    System.exit(1);
                });
                builder.create().show();
            }

        });
    }
}